chrome.webNavigation.onCompleted.addListener(function () {
  console.log("onCompleted event!");
});

var
  search_tab_id;

chrome.extension.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.action == 'open_search_tab') {
    if (search_tab_id) {
      chrome.tabs.update(search_tab_id, {
        url: "https://www.linkedin.com/in/kenneth-jakobsen-8191b9a/",
        active: true
      });
    } else {
      chrome.tabs.create({
        url: "html/headers.html",
        active: false
      }, function (tab) {
        search_tab_id = tab.id
      });
    }
  }
});