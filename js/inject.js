'use strict';

var le_dock, le_search, le_msg; // dock - ссылка на родителя блоков. В зависимости от открытой страницы search - блоки поиска, msg - блоки сообщений
var html_pane; // Наша встроенная панель
var html_code; // Код страницы из внешнего файла
var hrt_app;

function hrt_inject_any_vue_code() {
    var btn_click_in_js = document.getElementById('btn_click_in_js');
    var to_click = document.getElementById('mynetwork-tab-icon');
    console.log(btn_click_in_js);
    btn_click_in_js.onclick = function () {        
        to_click.click();
    };
}

function hrt_inject_html() {
    if (html_code && html_pane) {
        html_pane.innerHTML = html_code;
        hrt_app = null;
        hrt_app = new Vue({
            el: "#app",
            data: {
                message: "Vue is working!",
                switch1: false,
            },
            mounted: function () {
                this.$nextTick(function () {
                    hrt_inject_any_vue_code()
                });
            }
        });


        // chrome.browserAction.onClicked.addListener(function(tab) {
        //     chrome.debugger.attach({tabId:tab.id}, "1.0",
        //         onAttach.bind(null, tab.id));
        //   });

        console.log('injection finished...');
        if (le_search) {
            // chrome.extension.sendMessage({
            //     action: 'open_search_tab'
            // }, function () {});
        }
        // let project_container_temp = document.getElementById("project-container");
        // let project_container = project_container_temp.cloneNode(false); // empty container
        // project_container_temp.parentNode.replaceChild(project_container, project_container_temp);
        // project_container.
    }
}

function hrt_wait_page() {
    console.log('looking for injection... ' + document.title);
    if (document.getElementsByClassName('hrt_panel').length) {
        console.log('page is already injected...');
        return;
    }
    // document.getElementsByClassName("application-outlet")[0].id  = "app";
    //
    le_dock = le_search = le_msg = undefined;
    let le_grid = document.getElementsByClassName('neptune-grid two-column');
    if (le_grid && le_grid.length) {
        le_search = le_grid[0].getElementsByClassName('search-results-container');
        if (le_search && le_search.length) {
            le_search = le_search[0].getElementsByClassName('search-s-rail')[0].parentNode;
        } else {
            le_search = undefined;
        }
        if (!le_dock) {
            le_msg = document.getElementsByClassName('msg-messaging-container');
            if (le_msg && le_msg.length) {
                le_msg = le_msg[0];
            } else {
                le_msg = undefined;
            }
        }
        le_dock = le_search || le_msg;
        console.log(le_search);
        console.log(le_msg);
        console.log('*****le_dock:');
        console.log(le_dock);
        if (le_dock) {
            html_pane = document.createElement('aside');
            html_pane.className = "hrt_panel" + (le_search ? " hrt_search" : "") + (le_msg ? " hrt_msg" : "");
            // html_pane.innerHTML = ``;
            le_dock.appendChild(html_pane);
            hrt_inject_html();
        }
    }
}

function hrt_inject_controls() {
    timeoutHandle = undefined;
    if (le_search) { // search page
        let le_actions = document.getElementsByClassName('search-result__actions');
        if (le_actions && le_actions.length) {
            for (let lea of le_actions) {
                let le_button = lea.getElementsByClassName('hrt_mail_button');
                if (!le_button || !le_button.length) {
                    console.log(lea);
                    let html_button_mail = document.createElement('div');
                    html_button_mail.className = "hrt_mail_button p3 ember-view";
                    // html_button_mail.innerHTML = `<vs-switch color="dark" vs-icon-on="mail_outline" vs-icon-off="block" v-model="switch1">
                    //                                 <span slot="on">send</span>
                    //                                 <span slot="off">mail</span>
                    //                               </vs-switch>`;
                    html_button_mail.innerHTML = `<button class="search-result__action-button search-result__actions--primary artdeco-button artdeco-button--default artdeco-button--2 artdeco-button--secondary" data-control-name="srp_profile_actions" data-ember-action="" data-ember-action-151="151">
                                                Send mail
                                              </button>`;
                    // button.addEventListener("click", function() {
                    // alert(greeting + button.person_name + ".");
                    // }, false);
                    lea.appendChild(html_button_mail);
                }
            }
        }
    }
}

var timeoutHandle;

function nodeInsertedCallback(event) {
    if (timeoutHandle) clearTimeout(timeoutHandle);
    timeoutHandle = setTimeout(hrt_inject_controls, 200);
};

function hrt_first_init() {
    if (html_code) return;
    console.log('init DOM listener...');
    // document.addEventListener('DOMNodeInserted', nodeInsertedCallback);
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", chrome.extension.getURL("/html/inject.html"), true);
    xmlHttp.onload = function (e) {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            console.log('external html loaded...');
            html_code = xmlHttp.responseText;
            hrt_inject_html();
        } else {
            console.error(xmlHttp.statusText);
        }
    };
    xmlHttp.send(null);
}

function hrt_init() {
    console.log('waiting for body...');
    if (document.body) {
        hrt_wait_page();
    } else {
        setTimeout(hrt_init, 200);
    }
}

var hrt_previous_url;

function hrt_url_check() {
    if (window.location.toString() !== hrt_previous_url) {
        hrt_wait_page();
        hrt_previous_url = window.location.toString();
    }
    setTimeout(hrt_url_check, 500);
}

hrt_first_init();
hrt_init();
hrt_url_check();